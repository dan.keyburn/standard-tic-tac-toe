def print_board(entries):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in entries:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()

def game_over(board, n):
    print_board(board)
    print("GAME OVER")
    print(board[n], "has won")
    exit()

def is_row_winner(board, rowNum):
    if rowNum[0] == rowNum[1] and rowNum[1] == rowNum[2]:
        return True

def is_column_winner(board, columnNum):
    if columnNum[0] == columnNum[1] and columnNum[1] == columnNum[2]:
        return True

def is_diagonal_winner(board, diagonalNum):
    if diagonalNum[0] == diagonalNum[1] and diagonalNum[1] == diagonalNum[2]:
        return True

board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"

for move_number in range(1, 10):
    print_board(board)
    response = input("Where would " + current_player + " like to move? ")
    space_number = int(response) - 1
    board[space_number] = current_player

    top_row = (board[0], board[1], board[2])
    middle_row = (board[3], board[4], board[5])
    bottom_row = (board[6], board[7], board[8])
    column_left = (board[0], board[3], board[6])
    column_middle = (board[1], board[4], board[7])
    column_right = (board[2], board[5], board[8])
    left_diagonal = (board[0], board[4], board[8])
    right_diagonal = (board[2], board[4], board[6])

    if is_row_winner(board, top_row):
        game_over(board, 1)
    elif is_row_winner(board, middle_row):
        game_over(board, 2)
    elif is_row_winner(board, bottom_row):
        game_over(board, 6)
    elif is_column_winner(board, column_left):
        game_over(board, 0)
    elif is_column_winner(board, column_middle):
        game_over(board, 1)
    elif is_column_winner(board, column_right):
        game_over(board, 2)
    elif is_diagonal_winner(board, left_diagonal):
        game_over(board, 0)
    elif is_diagonal_winner(board, right_diagonal):
        game_over(board, 2)

    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie!")
